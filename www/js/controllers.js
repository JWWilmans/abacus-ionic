angular.module('numbering.controllers', [])

.controller('AbacusController', ['$scope', '$ionicModal', '$timeout', '$http', function($scope, $ionicModal, $timeout, $http) {

  // ensure we have directional swaps for our abacus
  $scope.right = true;
  $scope.swopDirection = function(dir) {
    if (dir === 'left') {
      $scope.right = false;
      console.log("we are now going right starting left");
      // keep styles the same just remove attrs
      goRight();
    } else {
      $scope.right = true;
      // abacus is reset right
      console.log("we are now going left starting right");
      goLeft();
    }
  }

  function goLeft(){
    // keep styles the same just remove attrs
    $(".left-numbers").hide();
    $(".right-numbers").show();
    var dragItem = document.getElementsByClassName('draggable');
    angular.forEach(dragItem, function(value, key) {
      $(dragItem[key]).removeAttr('style');
      $(dragItem[key]).attr("data-x", "0");
      $(dragItem[key]).css("float", "right");
    });
    doTable()
  }

  function goRight(){
    $(".left-numbers").show();
    $(".right-numbers").hide();
    var dragItem = document.getElementsByClassName('draggable');
    angular.forEach(dragItem, function(value, key) {
      $(dragItem[key]).removeAttr('style');
      $(dragItem[key]).attr("data-x", "0");
      $(dragItem[key]).css("float", "left");
    });
    doTable()
  }

  // start with right position
  goLeft();

  $scope.screenSize = window.screen.width;
  $scope.maxWidth = $scope.screenSize - (35 * 11);
  $ionicModal.fromTemplateUrl('templates/modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
    console.log('insuccess')
  });

  $scope.launchmodal = function() {
    console.log('starting')
    $scope.modal.show();
    console.log('finished')
  }

  $scope.goLeft = function() {
    //    console.log("going left!");
    $('.draggable').removeAttr('style');
  }

  $scope.mute = false;

  if ($scope.mute === true) {
    // todo mute sounds and swap icon
  }

  function doTable() {
    var dragItem = document.getElementsByClassName('draggable');
    var target, parent, globalNewTrain;
    angular.forEach(dragItem, function(value, key) {
      // get item being dragged
      var dragMe = dragItem[key];
      var className = dragItem[key].parentNode.className;
      var string = '$(".' + className + '")';
      var id = dragItem[key].id;
      // function wrap(el, wrapper) {
      //   el.parentNode.insertBefore(wrapper, el);
      //   wrapper.appendChild(el);
      // }

      interact(dragMe).draggable({
        snap: {
          targets: [
            // snap only the y coord to 100
            // i.e. move horizontally at y=100
            {
              y: 100,
              range: Infinity
            }
          ]
        },
        // enable inertial throwing
        //if true tiles can slide under eachother as they are no longer be dragged
        //so dragMoveListener is not called
        //need a inertia listerner if want this
        inertia: true,
        // keep the element within the area of it's parent
        restrict: {
          restriction: className,
          endOnly: true,
          elementRect: {
            top: 0,
            left: 0,
            bottom: 1,
            right: 1
          }
        },
        axis: 'xy',
        autoScroll: true,
        onend: function(event) {},
        onstart: function(event) {},
        onmove: dragMoveListener,
      });
    });
  }

  function moveMe(element, x, y, direction) {
    //setup boundaries
    //left the 0 cos going negative/left now
    //max widht * -1 to get a negative max width
    var inBoundary;
    if($scope.right === true){
      // to go from right to left
      inBoundary = x < 0 && x > $scope.maxWidth * -1;
    }else {
      inBoundary = x > 0 && x < $scope.maxWidth;
    }

//console.log(inBoundary + " = " + element.id) ;

    if (inBoundary) {

      element.style.webkitTransform =
      element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
      element.setAttribute('data-x', x);
      element.setAttribute('data-y', y);
      var positionOfMovedElement = getPositions(element);

      if($scope.right === true){
        var toTheRight = $(element).prev('div.draggable');
        var toTheLeft = $(element).next('div.draggable');
      }else {
        var toTheRight = $(element).next('div.draggable');
        var toTheLeft = $(element).prev('div.draggable');
      }

      var rightChild = toTheRight[0];

      //could be underfind if say is tile 40 has nothing to the right
      if (direction > 0 && typeof rightChild != 'undefined') {
        // RIGHT
        //    console.log("RIGHT");
        var obsticalPositionNext = getPositions(rightChild);
        var horizontalMatchNext = comparePositions(positionOfMovedElement[0], obsticalPositionNext[0]);
        //  var verticalMatchNext = comparePositions(positionOfMovedElement[1], obsticalPositionNext[1]);
        //var matchNext = horizontalMatchNext && verticalMatchNext;
        var collsionRight = horizontalMatchNext;
        if (collsionRight) {
          var nextX = (parseFloat(rightChild.getAttribute('data-x')) || 0) + direction;
          moveMe(rightChild, x, y, direction);
        }
      }

      var leftChild = toTheLeft[0];
      //console.log(leftChild);
      //could be underfind if say is tile 1 has nothign to the left
      if (direction < 0 && typeof leftChild != 'undefined') {
        //LEFT

        var obsticalPositionPervious = getPositions(leftChild);
        var horizontalMatchPervious = comparePositions(obsticalPositionPervious[0], positionOfMovedElement[0]);
        //var verticalMatchPervious = comparePositions(positionOfMovedElement[1], obsticalPositionPervious[1]);
        //var matchPervious = horizontalMatchPervious && verticalMatchPervious;
        var collsionLeft = horizontalMatchPervious;
        if (collsionLeft) {
          var prevX = (parseFloat(leftChild.getAttribute('data-x')) || 0) + direction;
          moveMe(leftChild, x, y, direction);
        }
      }
    } else {
        //else it has its its boundry, cant move
        //but
        //check if working with the "last" tile of the rwo
        //e.g. number 10,20,30
        //if is once of these
        //then check if they have reached their max left of right and then make the next row glow


   if($scope.right === true && x < 0){

//moveing right and x is less the zero so left boundry so must glow

theID = element.id;
//console.log(theID);
//if devisbale by 10
//then its one of the last
if (theID % 10 === 0) {
    //no point doing any thign for 100
    //console.log(theID);

        row = theID / 10;
        nextRow = row + 1;

        if (nextRow < 11) {
              obj = $("#" + nextRow);
              $("#icon_" + nextRow).find(".icon").css("opacity", "0");
              setTimeout(function() {
                $("#icon_" + nextRow).find(".icon").css("opacity", "1");
              }, 1000);
              console.log("make row "+nextRow+ " glow = " + obj);
              //console.log("make row "+nextRow+ " glow = " + obj);
          }
      }

      }
       if($scope.right === false && x > 0){


         //moveing left and x is greater the zero so right boundry so must glow
         //any right boundry was hit so last must have hit so can glow

          theID = element.id;
          decimal = theID / 10;
          nextRow = Math.ceil(decimal);
          nextRow++;

          if (nextRow < 11) {
            obj = $("#" + nextRow);
            $("#icon_" + nextRow).find(".icon").css("opacity", "0");
            setTimeout(function() {
              $("#icon_" + nextRow).find(".icon").css("opacity", "1");
            }, 1000);

            console.log("make row " + nextRow + " glow = " + obj);
          }

      }


    }
  }



  function dragMoveListener(event, h) {
    var target = event.target;

    // keep the dragged position in the data-x/data-y attributes
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
    moveMe(target, x, y, event.dx);
  }

  function getPositions(box) {
    var $box = $(box);
    var pos = $box.position();
    var width = $box.width();
    var height = $box.height();
    return [
      [pos.left, pos.left + width],
      [pos.top, pos.top + height]
    ];
  }

  function comparePositions(p1, p2) {
    if (p2[0] <= p1[1]) {
      //COLLISION
      //console.log("COLLISION");
      return true;
    } else {
      return false;
    }
  }

  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;

}]);
