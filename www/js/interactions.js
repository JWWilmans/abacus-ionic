interact('.tap-target1')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg1');
    event.preventDefault();
  });

interact('.tap-target2')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg2');
    event.preventDefault();
  });

interact('.tap-target3')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg3');
    event.preventDefault();
  });

interact('.tap-target4')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg4');
    event.preventDefault();
  });

interact('.tap-target5')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg5');
    event.preventDefault();
  });

interact('.tap-target6')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg6');
    event.preventDefault();
  });

interact('.tap-target7')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg7');
    event.preventDefault();
  });

interact('.tap-target8')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg8');
    event.preventDefault();
  });

interact('.tap-target9')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg9');
    event.preventDefault();
  });

interact('.tap-target10')
  .on('doubletap', function(event) {
    event.currentTarget.classList.toggle('switch-bg10');
    event.preventDefault();
  });

//sounds
interact('.one')
  .on('hold', function(event) {
    var audio = new Audio('audio/a1.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.two')
  .on('hold', function(event) {
    var audio = new Audio('audio/2.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.three')
  .on('hold', function(event) {
    var audio = new Audio('audio/3.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.four')
  .on('hold', function(event) {
    var audio = new Audio('audio/4.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.five')
  .on('hold', function(event) {
    var audio = new Audio('audio/5.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.six')
  .on('hold', function(event) {
    var audio = new Audio('audio/6.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seven')
  .on('hold', function(event) {
    var audio = new Audio('audio/7.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.eight')
  .on('hold', function(event) {
    var audio = new Audio('audio/8.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.nine')
  .on('hold', function(event) {
    var audio = new Audio('audio/9.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.ten')
  .on('hold', function(event) {
    var audio = new Audio('audio/10.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eleven')
  .on('hold', function(event) {
    var audio = new Audio('audio/11.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.twelve')
  .on('hold', function(event) {
    var audio = new Audio('audio/12.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.thirteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/13.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.fourteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/14.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fifteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/15.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/16.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventeen')
  .on('hold', function(event) {
    var audio = new Audio('audio/17.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eighteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/18.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.nineteen')
  .on('hold', function(event) {
    var audio = new Audio('audio/19.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.twenty')
  .on('hold', function(event) {
    var audio = new Audio('audio/20.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/21.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/22.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/23.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/24.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/25.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/26.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/27.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/28.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.twentynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/29.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirty')
  .on('hold', function(event) {
    var audio = new Audio('audio/30.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.thirtyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/31.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/32.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/33.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/34.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/35.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/36.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/37.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/38.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.thirtynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/39.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.forty')
  .on('hold', function(event) {
    var audio = new Audio('audio/40.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/41.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/42.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/43.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/44.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/45.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/46.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/47.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/48.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fortynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/49.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fifty')
  .on('hold', function(event) {
    var audio = new Audio('audio/50.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fiftyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/51.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fiftytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/52.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.fiftythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/53.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/54.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/55.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/56.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/57.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/58.mp3');
    audio.play();
    event.preventDefault();
  });
interact('.fiftynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/59.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixty')
  .on('hold', function(event) {
    var audio = new Audio('audio/60.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/61.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/62.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/63.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/64.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/65.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/66.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/67.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/68.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.sixtynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/69.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventy')
  .on('hold', function(event) {
    var audio = new Audio('audio/70.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/71.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/72.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/73.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/74.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/75.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/76.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/77.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/78.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.seventynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/79.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eighty')
  .on('hold', function(event) {
    var audio = new Audio('audio/80.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/81.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/82.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/83.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/84.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/85.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/86.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/87.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/88.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.eightynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/89.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninety')
  .on('hold', function(event) {
    var audio = new Audio('audio/90.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetyone')
  .on('hold', function(event) {
    var audio = new Audio('audio/91.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetytwo')
  .on('hold', function(event) {
    var audio = new Audio('audio/92.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetythree')
  .on('hold', function(event) {
    var audio = new Audio('audio/93.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetyfour')
  .on('hold', function(event) {
    var audio = new Audio('audio/94.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetyfive')
  .on('hold', function(event) {
    var audio = new Audio('audio/95.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetysix')
  .on('hold', function(event) {
    var audio = new Audio('audio/96.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetyseven')
  .on('hold', function(event) {
    var audio = new Audio('audio/97.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetyeight')
  .on('hold', function(event) {
    var audio = new Audio('audio/98.mp3');
    audio.play();
    event.preventDefault();
  });

interact('.ninetynine')
  .on('hold', function(event) {
    var audio = new Audio('audio/99.mp3');
    audio.play();
    event.preventDefault();
  });


interact('.onehundred')
  .on('hold', function(event) {
    var audio = new Audio('audio/100.mp3');
    audio.play();
    event.preventDefault();
  });
