# README #

Abacus App to replicate functionality of real world device.

Deliverables:

Sound of number plays on hold of relevant tile
Tile is 'pegged' on double-tap
Tile can slide on x-axis
Tiles have collision enabled to move in conjunction with tile that is being manipulated

### Libraries used ###

ionic 1
Angular 1
interact.js

### TODO ###

Add x-axis motion on touch
Add collision to boxes
tidy up libs folder for junk libs